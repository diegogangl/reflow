# ## BEGIN GPL LICENSE BLOCK ##
#
#   Copyright (C) Diego Gangl
#   <diego@sinestesia.co>
#
#   This program is free software; you can redistribute it and/or
#   modify it under the terms of the GNU General Public License
#   as published by the Free Software Foundation; either version 2
#   of the License, or (at your option) any later version.
#
#   This program is distributed in the hope that it will be useful,
#   but WITHOUT ANY WARRANTY; without even the implied warranty of
#   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#   GNU General Public License for more details.
#
#   You should have received a copy of the GNU General Public License
#   along with this program; if not, write to the Free Software Foundation,
#   Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
#
# ## END GPL LICENSE BLOCK ##


bl_info = {
    'name': 'Animation Reflow',
    'description': 'Recalculate keyframes for different fps',
    'author': 'Diego Gangl <diego@sinestesia.co>',
    'version': (1, 0, 0),
    'blender': (2, 80, 0),
    'location': 'Properties > Render',
    'category': 'Animation',
}
    
import re
    
import bpy
from bpy.props import BoolProperty, FloatProperty
from bpy.types import Operator


# -------------------------------------------------------------------------
# OPERATOR
# -------------------------------------------------------------------------

class RF_OT_Reflow(Operator):
    bl_idname = 'reflow.recalculate'
    bl_label = 'Change FPS'
    bl_description = 'Recalculate animations for a different fps'
    bl_options = {'REGISTER', 'UNDO'}


    fps_source: FloatProperty(name = 'Source FPS',
                            min = 1,
                            default = 24,
                            description = 'Original FPS to convert from')


    fps_dest: FloatProperty(name = 'Destination FPS',
                          min = 1,
                          default = 60,
                          description = 'Final FPS to convert to')

    do_actions: BoolProperty(name = 'Change actions',
                             description = 'Move keyframes in actions',
                             default = True)

    do_nla: BoolProperty(name = 'Fix NLA Tracks',
                         description = ('Change Start and End frames'
                                        ' For NLA Tracks'),
                         default = True)

    do_markers: BoolProperty(name = 'Change Markers',
                             description = 'Move markers\' frames',
                             default = True)


    do_markers_name: BoolProperty(name = 'Change Marker Names',
                                  description = ('Try to change markers'
                                                 ' default names'),
                                  default = True)


    do_fix_endframe: BoolProperty(name = 'Change end frame',
                                  description = ('Change scene\'s end'
                                                 ' end frame'),
                                  default = True)

    @classmethod
    def poll(cls, context):
        return bpy.data.actions

    def invoke(self, context, event):

        # Set default FPS from current FPS
        self.fps_source = context.scene.render.fps
        return context.window_manager.invoke_props_dialog(self)


    def draw(self, context):
        self.layout.separator()
        row = self.layout.row()

        row.prop(self, 'fps_source')
        row.label(text='', icon='FORWARD')
        row.prop(self, 'fps_dest')

        self.layout.separator()
        self.layout.row().label(text='Settings', icon='MODIFIER')

        self.layout.row().prop(self, 'do_nla')
        self.layout.row().prop(self, 'do_actions')
        self.layout.row().prop(self, 'do_markers')
        self.layout.row().prop(self, 'do_markers_name')
        self.layout.row().prop(self, 'do_fix_endframe')
        self.layout.separator()

    def keyframe_resample(self, curve, diff):
        """Resample every keyframe in a curve"""

        for keyframe in curve.keyframe_points:
            frame_original = keyframe.co[0]

            if frame_original != 0:
                keyframe.co[0] = frame_original // diff

    def fix_nla_length(self, track, diff):
        """Fix start and end frames for NLA tracks."""

        for strip in track.strips:
            strip.action_frame_start //= diff
            strip.action_frame_end //= diff

    def execute(self, context):

        scene = context.scene
        render = scene.render
        actions = bpy.data.actions
        markers = scene.timeline_markers
        objects = bpy.data.objects

        diff = self.fps_source / self.fps_dest

        if diff == 1:
            self.report({'WARNING'}, 'Source / Destination FPS are the same.')
            return {'CANCELLED'}

        # Set new FPS in scene properties
        render.fps = self.fps_dest

        # Fix endframe
        # ---------------------------------------------------------------------
        if self.do_fix_endframe:
            scene.frame_end = scene.frame_end // diff


        # Fix actions
        # ---------------------------------------------------------------------
        if self.do_actions:
            [self.keyframe_resample(curve, diff)
             for action in actions
             for curve in action.fcurves]


        # Fix NLA tracks
        # ---------------------------------------------------------------------
        if self.do_nla:
            [self.fix_nla_length(track, diff)
             for obj in objects
             for track in obj.animation_data.nla_tracks
             if obj.animation_data and obj.animation_data.use_nla]


        # Fix Markers
        # ---------------------------------------------------------------------
        if self.do_markers:
            for mark in markers:
                if mark.frame != 0:
                    new_frame = mark.frame // diff
                    mark.frame = new_frame

                    if self.do_markers_name:
                        regex = re.match('^F_[0-9]*$', mark.name)

                        if regex:
                            mark.name = 'F_{0}'.format(new_frame)


        return {'FINISHED'}


def reflow_button(self, context):
    """Button for dimensions panel."""

    col = self.layout.split()
    col.alignment = 'RIGHT'
    col.label(text='')
    col.operator('reflow.recalculate')


# -------------------------------------------------------------------------
# REGISTER
# -------------------------------------------------------------------------

def register():
    bpy.utils.register_class(RF_OT_Reflow)
    bpy.types.RENDER_PT_dimensions.append(reflow_button)
    

def unregister():
    bpy.utils.unregister_class(RF_OT_Reflow)
